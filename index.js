function createNewUser() {
  var newUser = {};

  var firstName = prompt("Введіть ім'я:");
  var lastName = prompt("Введіть прізвище:");

  newUser.firstName = firstName;
  newUser.lastName = lastName;

  newUser.getLogin = function () {
    var firstLetter = this.firstName.charAt(0).toLowerCase();
    return firstLetter + this.lastName.toLowerCase();
  };

  return newUser;
}

var user = createNewUser();

var login = user.getLogin();

console.log(login);

// Медот об'єкту, це функція яка належить об'єкту
// Будь який тип даних
// Це означає, що він використовуєтся з метою передачі інформації від крапки
